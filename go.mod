module gitlab.com/dev.taghinia/go-common

go 1.20

require github.com/redis/go-redis/v9 v9.0.5

require github.com/hablullah/go-juliandays v1.0.0 // indirect

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/hablullah/go-hijri v1.0.2
	github.com/yaa110/go-persian-calendar v1.1.4
	go.mongodb.org/mongo-driver v1.11.6
)
