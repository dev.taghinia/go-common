package API

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	"gitlab.com/dev.taghinia/go-common/errors"
	"gitlab.com/dev.taghinia/go-common/tools"
)

type Filter struct {
	Condition any
	Label     string
	Operation string
}

type InfoRequest struct {
	Service       string
	RequestFilter string
	PolicyFilter  string
	GroupFilter   string
	ServiceFilter []Filter
	Sort          string
	Skip          int64
	Limit         int64
	ClientToken   string
	UserToken     string
	Route         string
	Lang          string
	Ctx           context.Context
}

type ResponseOtp struct {
	Meta        map[string]any
	IsArrayData bool
}

type apiResponse struct {
	Error   *errors.ResponseErrors `json:"errors,omitempty"`
	Data    any                    `json:"data,omitempty"`
	Message string                 `json:"message,omitempty"`
	Meta    map[string]any         `json:"meta,omitempty"`
}

type message struct {
	Id         string            `json:"id" bson:"_id"`
	MessageKey string            `json:"message_key" bson:"message_key"`
	Title      map[string]string `json:"title" bson:"title"`
	Detail     map[string]string `json:"detail" bson:"detail"`
	Status     int               `json:"status" bson:"status"`
}

func createSkip(strSkip string) int64 {
	skip, err := strconv.ParseInt(strSkip, 10, 64)
	if err != nil {
		skip = 0
	}
	return skip
}

func createLimit(strLimit string) int64 {
	limit, err := strconv.ParseInt(strLimit, 10, 64)
	if err != nil {
		limit = 10
	}
	return limit
}

// Create a new instance of InfoRequest struct and set fields according to request
func New(request *http.Request, service, serviceVersion string) *InfoRequest {
	query := request.URL.Query()
	header := request.Header

	ctx := context.Background()
	req := &InfoRequest{}

	req.Service = service
	req.RequestFilter = query.Get("filter")
	req.Lang = header.Get("lang")
	req.Skip = createSkip(query.Get("skip"))
	req.Limit = createLimit(query.Get("limit"))
	req.Route = request.URL.Path
	req.ClientToken = header.Get("client")
	req.GroupFilter = query.Get("aggregation")
	req.Sort = query.Get("sort")
	req.UserToken = header.Get("id_token")
	req.Ctx = ctx

	return req
}

func CreateFilter(cond Filter) interface{} {
	switch cond.Operation {
	case "text":
		return bson.M{"$text": bson.M{"$search": cond.Condition.(string)}}
	case "Start With":
		return primitive.Regex{Pattern: "^" + cond.Condition.(string) + ".", Options: "i"}
	case "End With":
		return primitive.Regex{Pattern: ".*" + cond.Condition.(string) + "$", Options: "i"}
	case "Equal":
		return bson.M{"$eq": cond.Condition}
	case "Include":
		return primitive.Regex{Pattern: ".*" + cond.Condition.(string) + ".*", Options: "i"}
	case "Empty":
		return bson.M{"$exists": false}
	case "not Empty":
		return bson.M{"$exists": true}
	case "=":
		if fmt.Sprintf("%T", cond.Condition) == "[]interface {}" {
			return bson.M{"$in": cond.Condition}
		}
		return bson.M{"$eq": ConvertFilterCondition(cond.Condition)}
	case ">=":
		return bson.M{"$gte": ConvertFilterCondition(cond.Condition)}
	case "<=":
		return bson.M{"$lte": ConvertFilterCondition(cond.Condition)}
	case ">":
		return bson.M{"$gt": ConvertFilterCondition(cond.Condition)}
	case "<":
		return bson.M{"$lt": ConvertFilterCondition(cond.Condition)}
	case "!=":
		if fmt.Sprintf("%T", cond.Condition) == "[]interface {}" {
			return bson.M{"$nin": cond.Condition}
		}
		return bson.M{"$ne": ConvertFilterCondition(cond.Condition)}
	}
	return bson.M{}
}

func ConvertFilterCondition(condition any) any {
	switch condition.(type) {
	case string:
		switch tools.ConvertorType(condition.(string)) {
		case "func":
			return tools.FindFunc(condition.(string))
		case "string":
			return condition
		default:
			return condition
		}
	default:
		return condition
	}
}

func findMessage(messageKey string, lang string) string {
	strEntity := tools.GetValueFromShardCommonDb("Messages:" + messageKey)
	if strEntity == "" {
		return ""
	}
	m := &message{}
	err := json.Unmarshal([]byte(strEntity), m)
	if err != nil {
		return ""
	}
	detail, ok := m.Detail[lang]
	if !ok {
		return ""
	}
	return detail
}

func (r InfoRequest) NewResponse(data any, messageKey string,
	Error *errors.ResponseErrors, opt *ResponseOtp) *apiResponse {
	res := &apiResponse{}
	if messageKey != "" {
		messageDetail := findMessage(messageKey, r.Lang)
		res.Message = messageDetail
	}
	if data != nil {
		res.Data = data
	}
	if Error != nil {
		res.Error = Error
	}
	if opt != nil && Error == nil {
		res.Meta = opt.Meta
	}
	if opt.IsArrayData && Error == nil {
		res.Data = make([]map[string]any, 0)
	}

	return res
}
